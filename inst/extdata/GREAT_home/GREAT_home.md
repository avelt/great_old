<img src="images/logo.png" alt="drawing" width="150"/>

<h1><span style="color:#2b93e8">Welcome to GREAT application !</span></h1>

GREAT (GRape Expression ATlas) allows to analyse and visualize all public RNA-seq data from *Vitis vinifera* species. 

It allows to explore gene expression in about 2000 public RNA-seq samples with interactive heatmaps or expression tables. 

Also, this application performs some statistical analyses, like genes clustering or differential gene expression analysis !

**<span style="color:#2b93e8">Technical contacts :</span>**

For the public RNA-seq samples database :
Lauriane Renault - <a href="mailto:lauriane.renault@inrae.fr" class="email">lauriane.renault@inrae.fr</a>

For the RNA-seq data analysis workflow and the web application :
Amandine Velt - <a href="mailto:amandine.velt@inrae.fr" class="email">amandine.velt@inrae.fr</a>

**<span style="color:#2b93e8">Scientific contact :</span>**

Camille Rustenholz - <a href="mailto:camille.rustenholz@inrae.fr" class="email">camille.rustenholz@inrae.fr</a>

<h2><span style="color:#2b93e8">Quick tour of the application</span></h2>

<h3><span style="color:#2b93e8">GREAT database</span></h3>

<h3><span style="color:#2b93e8">Quality control table</span></h3>

<h3><span style="color:#2b93e8">Gene expression visualization</span></h3>

<h3><span style="color:#2b93e8">Gene expression table</span></h3>

<h3><span style="color:#2b93e8">Gene expression clustering</span></h3>

<h3><span style="color:#2b93e8">Differential gene expression analysis</span></h3>

<h2><span style="color:#2b93e8">Some wordclouds from GREAT database</span></h2>

<!-- <img src="images/Organ_wordcloud.png" alt="drawing" width="300"/>
<img src="images/Variety_wordcloud.png" alt="drawing" width="300"/>
<img src="images/Tissue_wordcloud.png" alt="drawing" width="300"/> 
<img src="images/Short_stage_wordcloud.png" alt="drawing" width="300"/>
<img src="images/Treatment_nature_wordcloud.png" alt="drawing" width="300"/> -->