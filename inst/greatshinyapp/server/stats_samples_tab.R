observeEvent(input$show_stats_table, {
  stats_samples=data.frame(fread(system.file("extdata", "all_samples_statistics.txt", package = "great"), sep="\t", header=T, check.names=FALSE))
  stats_samples_ok=stats_samples[,1:(length(stats_samples)-2)]
  stats_samples=stats_samples_ok
  colnames(stats_samples)=c("Samplename", "Number reads", "Average reads length", "Number of reads mapped after parsing", "% of reads mapped after parsing", "Number of reads/fragments assigned to VCost.v3_20 annotation genes", "% of reads/fragments assigned to VCost.v3_20 annotation genes")

  output$my_table_stats_all <- renderDT(
    datatable(stats_samples,filter = 'top',
              extensions = 'Buttons',
              options = list(
                dom = 'Brtip',
                sDom  = '<"top">lrt<"bottom">ip',
                pageLength = 20,
                buttons = list(
                  list(extend = "csv",
                  className = "btn btn-primary",
                  text = "Download filtered quality statistics table",
                  filename = paste("GREAT_quality_statistics_filtered", sep=""))
                ), initComplete = JS("function(){$('.dt-buttons button').removeClass('dt-button');}")
              ), rownames= FALSE
    )
  )

output$download_stats_samples <- downloadHandler(
    filename = function() {
      paste("GREAT_quality_statistics_", Sys.Date(), ".csv", sep="")
    },
    content = function(file) {
      write.csv(stats_samples, file)
    }
  )

output$download_stats_ui <- renderUI({
  downloadButton("download_stats_samples", "Download entire quality statistics table", class = "btn-primary")
})

})
