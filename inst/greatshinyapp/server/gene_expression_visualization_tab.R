#################################################################################
#--------------------------------------------------------------------------------
# CREATE CRIBI GRAPHICS

######################################
# Updating Treatment nature select Input
######################################
observe({
  if("Select All" %in% input$Treatment_type){
    x <- info_data_table$Treatment_nature[info_data_table$Treatment_type %in% vchoices_treatment_type]
    updateSelectInput(session,"Treatment_nature","Filter on 'Treatment nature' (*mandatory*)",choices = c("Select All",sort(as.character(unique(x)))))
  } else {
    x <- info_data_table$Treatment_nature[info_data_table$Treatment_type %in% input$Treatment_type]
    updateSelectInput(session,"Treatment_nature","Filter on 'Treatment nature' (*mandatory*)",choices = c("Select All",sort(as.character(unique(x)))))
  }
})

######################################
#Updating Variety select Input
######################################
observe({
  if(("Select All" %in% input$Treatment_nature) & ("Select All" %in% input$Treatment_type)){
    varietydata <- info_data_table$Variety[info_data_table$Treatment_nature %in% vchoices_treatment_nature]
    updateSelectInput(session,"Variety","Filter on 'Variety' (*mandatory*)",choices = c("Select All",sort(as.character(varietydata))))
  } else if (("Select All" %in% input$Treatment_nature) & !("Select All" %in% input$Treatment_type)) {
    varietydata <- info_data_table$Variety[info_data_table$Treatment_type %in% input$Treatment_type]
    updateSelectInput(session,"Variety","Filter on 'Variety' (*mandatory*)",choices = c("Select All",sort(as.character(varietydata))))
  } else if (!("Select All" %in% input$Treatment_nature) & ("Select All" %in% input$Treatment_type)) {
    varietydata <- info_data_table$Variety[info_data_table$Treatment_nature %in% input$Treatment_nature]
    updateSelectInput(session,"Variety","Filter on 'Variety' (*mandatory*)",choices = c("Select All",sort(as.character(varietydata))))
  } else {
    varietydata <- info_data_table$Variety[(info_data_table$Treatment_nature %in% input$Treatment_nature) & (info_data_table$Treatment_type %in% input$Treatment_type)]
    updateSelectInput(session,"Variety","Filter on 'Variety' (*mandatory*)",choices = c("Select All",sort(as.character(varietydata))))
  }
})

######################################
# Updating Organ select Input
######################################
observe({
  if(("Select All" %in% input$Variety) & ("Select All" %in% input$Treatment_nature) & ("Select All" %in% input$Treatment_type)){
    organdata <- info_data_table$Organ[info_data_table$Variety %in% vchoices_variety]
    updateSelectInput(session,"Organ","Filter on 'Organ' (*mandatory*)",choices = c("Select All", sort(as.character(organdata))))
  } else if (("Select All" %in% input$Variety) & !("Select All" %in% input$Treatment_nature) & !("Select All" %in% input$Treatment_type)) {
    organdata <- info_data_table$Organ[(info_data_table$Treatment_type %in% input$Treatment_type) & (info_data_table$Treatment_nature %in% input$Treatment_nature) ]
    updateSelectInput(session,"Organ","Filter on 'Organ' (*mandatory*)",choices = c("Select All", sort(as.character(organdata))))
  } else if (("Select All" %in% input$Variety) & !("Select All" %in% input$Treatment_nature) & ("Select All" %in% input$Treatment_type)) {
    organdata <- info_data_table$Organ[info_data_table$Treatment_nature %in% input$Treatment_nature]
    updateSelectInput(session,"Organ","Filter on 'Organ' (*mandatory*)",choices = c("Select All", sort(as.character(organdata))))
  } else if (("Select All" %in% input$Variety) & ("Select All" %in% input$Treatment_nature) & !("Select All" %in% input$Treatment_type)) {
    organdata <- info_data_table$Organ[info_data_table$Treatment_type %in% input$Treatment_type]
    updateSelectInput(session,"Organ","Filter on 'Organ' (*mandatory*)",choices = c("Select All", sort(as.character(organdata))))
  } else if (!("Select All" %in% input$Variety) & ("Select All" %in% input$Treatment_nature) & !("Select All" %in% input$Treatment_type)) {
    organdata <- info_data_table$Organ[(info_data_table$Variety %in% input$Variety) & (info_data_table$Treatment_type %in% input$Treatment_type)]
    updateSelectInput(session,"Organ","Filter on 'Organ' (*mandatory*)",choices = c("Select All", sort(as.character(organdata))))
  } else if (!("Select All" %in% input$Variety) & !("Select All" %in% input$Treatment_nature) & ("Select All" %in% input$Treatment_type)) {
    organdata <- info_data_table$Organ[(info_data_table$Variety %in% input$Variety) & (info_data_table$Treatment_nature %in% input$Treatment_nature)]
    updateSelectInput(session,"Organ","Filter on 'Organ' (*mandatory*)",choices = c("Select All", sort(as.character(organdata))))
  } else if (!("Select All" %in% input$Variety) & !("Select All" %in% input$Treatment_nature) & !("Select All" %in% input$Treatment_type)) {
    organdata <- info_data_table$Organ[(info_data_table$Variety %in% input$Variety) & (info_data_table$Treatment_nature %in% input$Treatment_nature)  & (info_data_table$Treatment_type %in% input$Treatment_type)]
    updateSelectInput(session,"Organ","Filter on 'Organ' (*mandatory*)",choices = c("Select All", sort(as.character(organdata))))
  } else {
    organdata <- info_data_table$Organ[info_data_table$Variety %in% input$Variety]
    updateSelectInput(session,"Organ","Filter on 'Organ' (*mandatory*)",choices = c("Select All", sort(as.character(organdata))))
  }
})

######################################
# Updating Bioproject select Input
######################################
observe({
  if(("Select All" %in% input$Variety) & ("Select All" %in% input$Treatment_nature) & ("Select All" %in% input$Treatment_type) & ("Select All" %in% input$Organ)){
    bioprojectdata <- info_data_table$BioProject[info_data_table$Organ %in% vchoices_organ]
    updateSelectInput(session,"Bioproject","Filter on 'Bioproject' (*mandatory*)",choices = c("Select All", sort(as.character(bioprojectdata))))
  } else if (("Select All" %in% input$Variety) & !("Select All" %in% input$Treatment_nature) & !("Select All" %in% input$Treatment_type) & !("Select All" %in% input$Organ)) {
    bioprojectdata <- info_data_table$BioProject[(info_data_table$Organ %in% input$Organ) & (info_data_table$Treatment_type %in% input$Treatment_type) & (info_data_table$Treatment_nature %in% input$Treatment_nature) ]
    updateSelectInput(session,"Bioproject","Filter on 'Bioproject' (*mandatory*)",choices = c("Select All", sort(as.character(bioprojectdata))))
  } else if (("Select All" %in% input$Variety) & !("Select All" %in% input$Treatment_nature) & ("Select All" %in% input$Treatment_type ) & !("Select All" %in% input$Organ)) {
    bioprojectdata <- info_data_table$BioProject[(info_data_table$Organ %in% input$Organ) & (info_data_table$Treatment_nature %in% input$Treatment_nature)]
    updateSelectInput(session,"Bioproject","Filter on 'Bioproject' (*mandatory*)",choices = c("Select All", sort(as.character(bioprojectdata))))
  } else if (("Select All" %in% input$Variety) & ("Select All" %in% input$Treatment_nature) & !("Select All" %in% input$Treatment_type ) & !("Select All" %in% input$Organ)) {
    bioprojectdata <- info_data_table$BioProject[(info_data_table$organ %in% input$Organ) & (info_data_table$Treatment_type %in% input$Treatment_type)]
    updateSelectInput(session,"Bioproject","Filter on 'Bioproject' (*mandatory*)",choices = c("Select All", sort(as.character(bioprojectdata))))
  } else if (!("Select All" %in% input$Variety) & ("Select All" %in% input$Treatment_nature) & !("Select All" %in% input$Treatment_type) & !("Select All" %in% input$Organ)) {
    bioprojectdata <- info_data_table$BioProject[(info_data_table$Organ %in% input$Organ) & (info_data_table$Treatment_type %in% input$Treatment_type) & (info_data_table$Variety %in% input$Variety)]
    updateSelectInput(session,"Bioproject","Filter on 'Bioproject' (*mandatory*)",choices = c("Select All", sort(as.character(bioprojectdata))))
  } else if (!("Select All" %in% input$Variety) & !("Select All" %in% input$Treatment_nature) & ("Select All" %in% input$Treatment_type) & !("Select All" %in% input$Organ)) {
    bioprojectdata <- info_data_table$BioProject[(info_data_table$Organ %in% input$Organ) & (info_data_table$Variety %in% input$Variety) & (info_data_table$Treatment_nature %in% input$Treatment_nature)]
    updateSelectInput(session,"Bioproject","Filter on 'Bioproject' (*mandatory*)",choices = c("Select All", sort(as.character(bioprojectdata))))
  } else if (!("Select All" %in% input$Variety) & !("Select All" %in% input$Treatment_nature) & !("Select All" %in% input$Treatment_type) & !("Select All" %in% input$Organ)) {
    bioprojectdata <- info_data_table$BioProject[(info_data_table$Organ %in% input$Organ) &(info_data_table$Variety %in% input$Variety) & (info_data_table$Treatment_nature %in% input$Treatment_nature)  & (info_data_table$Treatment_type %in% input$Treatment_type)]
    updateSelectInput(session,"Bioproject","Filter on 'Bioproject' (*mandatory*)",choices = c("Select All", sort(as.character(bioprojectdata))))
  } else if (("Select All" %in% input$Variety) & !("Select All" %in% input$Treatment_nature) & !("Select All" %in% input$Treatment_type) & ("Select All" %in% input$Organ)) {
    bioprojectdata <- info_data_table$BioProject[(info_data_table$Treatment_type %in% input$Treatment_type) & (info_data_table$Treatment_nature %in% input$Treatment_nature) ]
    updateSelectInput(session,"Bioproject","Filter on 'Bioproject' (*mandatory*)",choices = c("Select All", sort(as.character(bioprojectdata))))
  } else if (("Select All" %in% input$Variety) & !("Select All" %in% input$Treatment_nature) & ("Select All" %in% input$Treatment_type) & ("Select All" %in% input$Organ)) {
    bioprojectdata <- info_data_table$BioProject[info_data_table$Treatment_nature %in% input$Treatment_nature]
    updateSelectInput(session,"Bioproject","Filter on 'Bioproject' (*mandatory*)",choices = c("Select All", sort(as.character(bioprojectdata))))
  } else if (("Select All" %in% input$Variety) & ("Select All" %in% input$Treatment_nature) & !("Select All" %in% input$Treatment_type) & ("Select All" %in% input$Organ)) {
    bioprojectdata <- info_data_table$BioProject[info_data_table$Treatment_type %in% input$Treatment_type]
    updateSelectInput(session,"Bioproject","Filter on 'Bioproject' (*mandatory*)",choices = c("Select All", sort(as.character(bioprojectdata))))
  } else if (!("Select All" %in% input$Variety) & ("Select All" %in% input$Treatment_nature) & !("Select All" %in% input$Treatment_type) & ("Select All" %in% input$Organ)) {
    bioprojectdata <- info_data_table$BioProject[(info_data_table$Variety %in% input$Variety) & (info_data_table$Treatment_type %in% input$Treatment_type)]
    updateSelectInput(session,"Bioproject","Filter on 'Bioproject' (*mandatory*)",choices = c("Select All", sort(as.character(bioprojectdata))))
  } else if (!("Select All" %in% input$Variety) & !("Select All" %in% input$Treatment_nature) & ("Select All" %in% input$Treatment_type) & ("Select All" %in% input$Organ)) {
    bioprojectdata <- info_data_table$BioProject[(info_data_table$Variety %in% input$Variety) & (info_data_table$Treatment_nature %in% input$Treatment_nature)]
    updateSelectInput(session,"Bioproject","Filter on 'Bioproject' (*mandatory*)",choices = c("Select All", sort(as.character(bioprojectdata))))
  } else if (!("Select All" %in% input$Variety) & !("Select All" %in% input$Treatment_nature) & !("Select All" %in% input$Treatment_type) & ("Select All" %in% input$Organ)) {
    bioprojectdata <- info_data_table$BioProject[(info_data_table$Variety %in% input$Variety) & (info_data_table$Treatment_nature %in% input$Treatment_nature)  & (info_data_table$Treatment_type %in% input$Treatment_type)]
    updateSelectInput(session,"Bioproject","Filter on 'Bioproject' (*mandatory*)",choices = c("Select All", sort(as.character(bioprojectdata))))
  } else {
    bioprojectdata <- info_data_table$BioProject[info_data_table$Organ %in% input$Organ]
    updateSelectInput(session,"Bioproject","Filter on 'Bioproject' (*mandatory*)",choices = c("Select All", sort(as.character(bioprojectdata))))
  }
})

observeEvent(input$reset_cribi_plots, {
  reset("Bioproject")
  reset("Treatment_type")
  reset("Treatment_nature")
  reset("Variety")
  reset("Organ")
})

observeEvent(input$launch, {
  message("Running code...")
  if("Select All" %in% input$Bioproject & "Select All" %in% input$Variety & "Select All" %in% input$Treatment_nature & "Select All" %in% input$Treatment_type & "Select All" %in% input$Organ){
    samples_filtered=subset(info_data_table)[,4]
  } else if (!("Select All" %in% input$Bioproject) & !("Select All" %in% input$Variety) & !("Select All" %in% input$Treatment_nature) & !("Select All" %in% input$Treatment_type)& "Select All" %in% input$Organ){
    samples_filtered=subset(info_data_table, BioProject %in% input$Bioproject & Variety %in% input$Variety & Treatment_nature %in% input$Treatment_nature & Treatment_type %in% input$Treatment_type)[,4]
  } else if ("Select All" %in% input$Bioproject & !("Select All" %in% input$Variety) & !("Select All" %in% input$Treatment_nature) & !("Select All" %in% input$Treatment_type)& "Select All" %in% input$Organ){
    samples_filtered=subset(info_data_table, Variety %in% input$Variety & Treatment_nature %in% input$Treatment_nature & Treatment_type %in% input$Treatment_type)[,4]
  } else if ("Select All" %in% input$Bioproject & "Select All" %in% input$Variety & !("Select All" %in% input$Treatment_nature) & !("Select All" %in% input$Treatment_type)& "Select All" %in% input$Organ){
    samples_filtered=subset(info_data_table, Treatment_nature %in% input$Treatment_nature & Treatment_type %in% input$Treatment_type)[,4]
  } else if ("Select All" %in% input$Bioproject & "Select All" %in% input$Variety & "Select All" %in% input$Treatment_nature & !("Select All" %in% input$Treatment_type)& "Select All" %in% input$Organ){
    samples_filtered=subset(info_data_table, Treatment_type %in% input$Treatment_type)[,4]
  } else if ("Select All" %in% input$Bioproject & "Select All" %in% input$Variety & !("Select All" %in% input$Treatment_nature) & "Select All" %in% input$Treatment_type & "Select All" %in% input$Organ){
    samples_filtered=subset(info_data_table, Treatment_nature %in% input$Treatment_nature)[,4]
  } else if ("Select All" %in% input$Bioproject & !("Select All" %in% input$Variety) & "Select All" %in% input$Treatment_nature & "Select All" %in% input$Treatment_type & "Select All" %in% input$Organ){
    samples_filtered=subset(info_data_table, Variety %in% input$Variety)[,4]
  } else if (!("Select All" %in% input$Bioproject) & "Select All" %in% input$Variety & "Select All" %in% input$Treatment_nature & "Select All" %in% input$Treatment_type & "Select All" %in% input$Organ){
    samples_filtered=subset(info_data_table, BioProject %in% input$Bioproject)[,4]
  } else if (!("Select All" %in% input$Bioproject) & !("Select All" %in% input$Variety) & "Select All" %in% input$Treatment_nature & !("Select All" %in% input$Treatment_type)& "Select All" %in% input$Organ){
    samples_filtered=subset(info_data_table, BioProject %in% input$Bioproject & Variety %in% input$Variety & Treatment_type %in% input$Treatment_type)[,4]
  } else if (!("Select All" %in% input$Bioproject) & !("Select All" %in% input$Variety) & !("Select All" %in% input$Treatment_nature) & "Select All" %in% input$Treatment_type & "Select All" %in% input$Organ){
    samples_filtered=subset(info_data_table, BioProject %in% input$Bioproject & Variety %in% input$Variety & Treatment_nature %in% input$Treatment_nature)[,4]
  } else if (!("Select All" %in% input$Bioproject) & !("Select All" %in% input$Variety) & "Select All" %in% input$Treatment_nature & "Select All" %in% input$Treatment_type & "Select All" %in% input$Organ){
    samples_filtered=subset(info_data_table, BioProject %in% input$Bioproject & Variety %in% input$Variety)[,4]
  } else if ("Select All" %in% input$Bioproject & !("Select All" %in% input$Variety) & !("Select All" %in% input$Treatment_nature) & "Select All" %in% input$Treatment_type & "Select All" %in% input$Organ){
    samples_filtered=subset(info_data_table, Treatment_nature %in% input$Treatment_nature & Variety %in% input$Variety)[,4]
  } else if ("Select All" %in% input$Bioproject & !("Select All" %in% input$Variety) & "Select All" %in% input$Treatment_nature & !("Select All" %in% input$Treatment_type) & "Select All" %in% input$Organ){
    samples_filtered=subset(info_data_table, Treatment_type %in% input$Treatment_type & Variety %in% input$Variety)[,4]
  } else if (!("Select All" %in% input$Bioproject) & "Select All" %in% input$Variety & "Select All" %in% input$Treatment_nature & !("Select All" %in% input$Treatment_type) & "Select All" %in% input$Organ){
    samples_filtered=subset(info_data_table, BioProject %in% input$Bioproject & Treatment_type %in% input$Treatment_type)[,4]
  } else if (!("Select All" %in% input$Bioproject) & "Select All" %in% input$Variety & !("Select All" %in% input$Treatment_nature) & "Select All" %in% input$Treatment_type & "Select All" %in% input$Organ){
    samples_filtered=subset(info_data_table, BioProject %in% input$Bioproject & Treatment_nature %in% input$Treatment_nature)[,4]
  } else if ("Select All" %in% input$Bioproject & "Select All" %in% input$Variety & !("Select All" %in% input$Treatment_nature) & !("Select All" %in% input$Treatment_type) & "Select All" %in% input$Organ){
    samples_filtered=subset(info_data_table, Treatment_nature %in% input$Treatment_nature & Treatment_type %in% input$Treatment_type)[,4]
  } else if (!("Select All" %in% input$Bioproject) & "Select All" %in% input$Variety & !("Select All" %in% input$Treatment_nature) & !("Select All" %in% input$Treatment_type) & "Select All" %in% input$Organ){
    samples_filtered=subset(info_data_table, BioProject %in% input$Bioproject & Treatment_type %in% input$Treatment_type & Treatment_nature %in% input$Treatment_nature)[,4]
  } else if (!("Select All" %in% input$Bioproject) & !("Select All" %in% input$Variety) & !("Select All" %in% input$Treatment_nature) & !("Select All" %in% input$Treatment_type) & !("Select All" %in% input$Organ)){
    samples_filtered=subset(info_data_table, Organ %in% input$Organ & BioProject %in% input$Bioproject & Variety %in% input$Variety & Treatment_nature %in% input$Treatment_nature & Treatment_type %in% input$Treatment_type)[,4]
  } else if ("Select All" %in% input$Bioproject & !("Select All" %in% input$Variety) & !("Select All" %in% input$Treatment_nature) & !("Select All" %in% input$Treatment_type) & !("Select All" %in% input$Organ)){
    samples_filtered=subset(info_data_table, Organ %in% input$Organ & Variety %in% input$Variety & Treatment_nature %in% input$Treatment_nature & Treatment_type %in% input$Treatment_type)[,4]
  } else if ("Select All" %in% input$Bioproject & "Select All" %in% input$Variety & !("Select All" %in% input$Treatment_nature) & !("Select All" %in% input$Treatment_type) & !("Select All" %in% input$Organ)){
    samples_filtered=subset(info_data_table, Organ %in% input$Organ & Treatment_nature %in% input$Treatment_nature & Treatment_type %in% input$Treatment_type)[,4]
  } else if ("Select All" %in% input$Bioproject & "Select All" %in% input$Variety & "Select All" %in% input$Treatment_nature & !("Select All" %in% input$Treatment_type) & !("Select All" %in% input$Organ)){
    samples_filtered=subset(info_data_table, Organ %in% input$Organ & Treatment_type %in% input$Treatment_type)[,4]
  } else if ("Select All" %in% input$Bioproject & "Select All" %in% input$Variety & !("Select All" %in% input$Treatment_nature) & "Select All" %in% input$Treatment_type & !("Select All" %in% input$Organ)){
    samples_filtered=subset(info_data_table, Organ %in% input$Organ & Treatment_nature %in% input$Treatment_nature)[,4]
  } else if ("Select All" %in% input$Bioproject & !("Select All" %in% input$Variety) & "Select All" %in% input$Treatment_nature & "Select All" %in% input$Treatment_type & !("Select All" %in% input$Organ)){
    samples_filtered=subset(info_data_table, Organ %in% input$Organ & Variety %in% input$Variety)[,4]
  } else if (!("Select All" %in% input$Bioproject) & "Select All" %in% input$Variety & "Select All" %in% input$Treatment_nature & "Select All" %in% input$Treatment_type & !("Select All" %in% input$Organ)){
    samples_filtered=subset(info_data_table, Organ %in% input$Organ & BioProject %in% input$Bioproject)[,4]
  } else if (!("Select All" %in% input$Bioproject) & !("Select All" %in% input$Variety) & "Select All" %in% input$Treatment_nature & !("Select All" %in% input$Treatment_type) & !("Select All" %in% input$Organ)){
    samples_filtered=subset(info_data_table, Organ %in% input$Organ & BioProject %in% input$Bioproject & Variety %in% input$Variety & Treatment_type %in% input$Treatment_type)[,4]
  } else if (!("Select All" %in% input$Bioproject) & !("Select All" %in% input$Variety) & !("Select All" %in% input$Treatment_nature) & "Select All" %in% input$Treatment_type & !("Select All" %in% input$Organ)){
    samples_filtered=subset(info_data_table, Organ %in% input$Organ & BioProject %in% input$Bioproject & Variety %in% input$Variety & Treatment_nature %in% input$Treatment_nature)[,4]
  } else if (!("Select All" %in% input$Bioproject) & !("Select All" %in% input$Variety) & "Select All" %in% input$Treatment_nature & "Select All" %in% input$Treatment_type & !("Select All" %in% input$Organ)){
    samples_filtered=subset(info_data_table, Organ %in% input$Organ & BioProject %in% input$Bioproject & Variety %in% input$Variety)[,4]
  } else if ("Select All" %in% input$Bioproject & "Select All" %in% input$Variety & "Select All" %in% input$Treatment_nature & "Select All" %in% input$Treatment_type & !("Select All" %in% input$Organ)){
    samples_filtered=subset(info_data_table, Organ %in% input$Organ)[,4]
  } else if ("Select All" %in% input$Bioproject & "Select All" %in% input$Variety & "Select All" %in% input$Treatment_nature & !("Select All" %in% input$Treatment_type) & "Select All" %in% input$Organ){
    samples_filtered=subset(info_data_table, Treatment_type %in% input$Treatment_type)[,4]
  } else if ("Select All" %in% input$Bioproject & "Select All" %in% input$Variety & !("Select All" %in% input$Treatment_nature) & "Select All" %in% input$Treatment_type & "Select All" %in% input$Organ){
    samples_filtered=subset(info_data_table, Treatment_nature %in% input$Treatment_nature)[,4]
  } else if ("Select All" %in% input$Bioproject & !("Select All" %in% input$Variety) & "Select All" %in% input$Treatment_nature & "Select All" %in% input$Treatment_type & "Select All" %in% input$Organ){
    samples_filtered=subset(info_data_table, Variety %in% input$Variety)[,4]
  } else if ("Select All" %in% input$Bioproject & !("Select All" %in% input$Variety) & !("Select All" %in% input$Treatment_nature) & "Select All" %in% input$Treatment_type & !("Select All" %in% input$Organ)){
    samples_filtered=subset(info_data_table, Organ %in% input$Organ & Treatment_nature %in% input$Treatment_nature & Variety %in% input$Variety)[,4]
  } else if ("Select All" %in% input$Bioproject & !("Select All" %in% input$Variety) & "Select All" %in% input$Treatment_nature & !("Select All" %in% input$Treatment_type) & !("Select All" %in% input$Organ)){
    samples_filtered=subset(info_data_table, Organ %in% input$Organ & Treatment_type %in% input$Treatment_type & Variety %in% input$Variety)[,4]
  } else if (!("Select All" %in% input$Bioproject) & "Select All" %in% input$Variety & "Select All" %in% input$Treatment_nature & !("Select All" %in% input$Treatment_type) & !("Select All" %in% input$Organ)){
    samples_filtered=subset(info_data_table, Organ %in% input$Organ & BioProject %in% input$Bioproject & Treatment_type %in% input$Treatment_type)[,4]
  } else if (!("Select All" %in% input$Bioproject) & "Select All" %in% input$Variety & !("Select All" %in% input$Treatment_nature) & "Select All" %in% input$Treatment_type & !("Select All" %in% input$Organ)){
    samples_filtered=subset(info_data_table, Organ %in% input$Organ & BioProject %in% input$Bioproject & Treatment_nature %in% input$Treatment_nature)[,4]
  } else if ("Select All" %in% input$Bioproject & "Select All" %in% input$Variety & !("Select All" %in% input$Treatment_nature) & !("Select All" %in% input$Treatment_type) & !("Select All" %in% input$Organ)){
    samples_filtered=subset(info_data_table, Organ %in% input$Organ & Treatment_nature %in% input$Treatment_nature & Treatment_type %in% input$Treatment_type)[,4]
  } else if (!("Select All" %in% input$Bioproject) & "Select All" %in% input$Variety & !("Select All" %in% input$Treatment_nature) & !("Select All" %in% input$Treatment_type) & !("Select All" %in% input$Organ)){
    samples_filtered=subset(info_data_table, Organ %in% input$Organ & BioProject %in% input$Bioproject & Treatment_type %in% input$Treatment_type & Treatment_nature %in% input$Treatment_nature)[,4]
  }

  data_info_CRIBI_plots=merge(as.data.frame(samples_filtered), info_data_table, all.x=T, by.y="File_name", by.x="samples_filtered")
  output$title_my_table_dataall_CRIBI_info_plots <- renderUI({
    HTML(
      paste(capture.output(type = "message", expr = {
        message(capture.output(type = "output", expr = {
          cat("<b><font size='4'>Info table of selected samples :</font></b>")
        }))
      }), collapse="<br>")
    )
  })

  output$my_table_dataall_CRIBI_info_plots <- renderDT(
    datatable(data_info_CRIBI_plots, filter = 'top',
              extensions = 'Buttons',
              options = list(
                dom = 'Brtip',
                sDom  = '<"top">lrt<"bottom">ip',
                pageLength = 30,
                buttons = list(
                  list(extend = "csv",
                  className = "btn btn-primary",
                  text = "Download filtered info table",
                  filename = paste("info_table_plots_filtered", sep=""))
                ), initComplete = JS("function(){$('.dt-buttons button').removeClass('dt-button');}")
              )
    )
  )

output$download_table_info_cribi_plots <- downloadHandler(
    filename = function() {
      paste("info_table_plots_", Sys.Date(), ".csv", sep="")
    },
    content = function(file) {
      write.csv(data_info_CRIBI_plots, file)
    }
  )

output$download_table_info_cribi_plots_ui <- renderUI({
  downloadButton("download_table_info_cribi_plots", "Download entire info table", class = "btn-primary")
})


  #################################################################################
  library("dplyr")
  if (length(samples_filtered) == 0 ){
    print("No samples correspond to your filter.")
  } else if (length(samples_filtered) == "NULL" ){
    print("No samples correspond to your filter.")
  } else if (length(samples_filtered) == 1 ){
    print("Only one sample selected, no clustering is possible.")
  } else {
    samples_filtered_ok <- c("gene_id",as.character(samples_filtered))
    data_CRIBI_samples <- as.data.frame(read_feather(system.file("extdata","RPKN_counts_all_genes.feather",package="great"), columns=samples_filtered_ok))
    data_CRIBI_samples2 <- data_CRIBI_samples[,-1]
    rownames(data_CRIBI_samples2) <- data_CRIBI_samples[,1]
    data_CRIBI_samples=data_CRIBI_samples2
    col_names=colnames(data_CRIBI_samples)
    for (name in colnames(data_CRIBI_samples)){
      name_ok=subset(info_data_table, File_name %in% name)[,c(4,15,12,13)]
      name_ok2=lapply(name_ok, function(x) if(is.factor(x)) as.character(x) else x)
      name_ok3=paste(name_ok2, collapse='||' )
      col_names=gsub(name, name_ok3, col_names)
    }
    colnames(data_CRIBI_samples)=make.names(names=col_names, unique = FALSE, allow_ = TRUE)
    data_CRIBI_samples_log <- log(data_CRIBI_samples+1)
    colnames(data_CRIBI_samples) <- gsub("\\.\\.", " || ", colnames(data_CRIBI_samples))
    colnames(data_CRIBI_samples_log) <- gsub("\\.\\.", " ", colnames(data_CRIBI_samples_log))
    colnames(data_CRIBI_samples) <- gsub("\\.", " ", colnames(data_CRIBI_samples))
    colnames(data_CRIBI_samples_log) <- gsub("\\.", " ", colnames(data_CRIBI_samples_log))
    colnames(data_CRIBI_samples_log) <- gsub("-", "_", colnames(data_CRIBI_samples_log))
    colnames(data_CRIBI_samples_log) <- gsub(" ", "_", colnames(data_CRIBI_samples_log))
    output$title_heatmap_CRIBI <- renderUI({
      HTML(
        paste(capture.output(type = "message", expr = {
          message(capture.output(type = "output", expr = {
            cat("<b><font size='6'>Heatmap on genes expression (RPKM).</font></b>")
          }))
        }), collapse="<br>")
      )})
      output$heatmap_CRIBI <- renderD3heatmap({
        logInput <- reactive({
        switch(input$log_or_not,
          "RPKM" = "RPKM",
          "LOG" = "LOG")
        })
        log_option=logInput()
        if (log_option == "RPKM") {
          Genes_list_CRIBI=input$Genes_list_CRIBI
          chr_CRIBI=input$chr_CRIBI
          start_CRIBI=input$start_CRIBI
          end_CRIBI=input$end_CRIBI
          strand_CRIBI=input$strand_CRIBI
          if(Genes_list_CRIBI == 'Chromosome coordinates' && chr_CRIBI != "" && start_CRIBI != "" && end_CRIBI != "" && strand_CRIBI != ""){
            print(chr_CRIBI)
            print(start_CRIBI)
            print(end_CRIBI)
            regions.gr_CRIBI <- GRanges(seqnames=chr_CRIBI, ranges=IRanges(start=as.numeric(start_CRIBI), end=as.numeric(end_CRIBI)), strand=strand_CRIBI)
            # overlap entre gff et région donnée
            overlapGenes_CRIBI <- findOverlaps(regions.gr_CRIBI, gffRangedData_CRIBI)
            overlapGenes.df_CRIBI <- as.data.frame(overlapGenes_CRIBI)
            # liste des gènes dans la région donnée
            genes_CRIBI=unique(names(gffRangedData_CRIBI[overlapGenes.df_CRIBI$subjectHits]))
            output$selected_genes_CRIBI <- renderText({
              paste(as.character(genes_CRIBI), collapse=",")
            })
            data_CRIBI_samples <- data_CRIBI_samples[row.names(data_CRIBI_samples) %in% genes_CRIBI, ]
            heatmaply(data_CRIBI_samples,dendrogram = "none",margins=c((max(nchar(colnames(data_CRIBI_samples)))*4),150+(length(colnames(data_CRIBI_samples))/20),2,2),fontsize_row=7,fontsize_col=7,colors = colorRampPalette(rev(brewer.pal(n = 7, name = "RdYlBu")))(100),labCol=colnames(data_CRIBI_samples),labRow=rownames(data_CRIBI_samples)) %>% layout(height=as.numeric(input$dimension[2]),width=(0.8*as.numeric(input$dimension[1])))
          } else {
            genes_CRIBI=unlist(strsplit(input$list_CRIBI,','))
            if(length(genes_CRIBI) != 0){
              data_CRIBI_samples <- data_CRIBI_samples[row.names(data_CRIBI_samples) %in% genes_CRIBI, ]
              heatmaply(data_CRIBI_samples,dendrogram = "none",margins=c((max(nchar(colnames(data_CRIBI_samples)))*4),150+(length(colnames(data_CRIBI_samples))/20),2,2),fontsize_row=7,fontsize_col=7,colors = colorRampPalette(rev(brewer.pal(n = 7, name = "RdYlBu")))(100),labCol=colnames(data_CRIBI_samples),labRow=rownames(data_CRIBI_samples)) %>% layout(height=as.numeric(input$dimension[2]),width=(0.8*as.numeric(input$dimension[1])))
            } else {
              shinyalert(title = "You genes ID list is empty, please fill it.", type = "warning")
            }
          }
        } else {
          Genes_list_CRIBI=input$Genes_list_CRIBI
          chr_CRIBI=input$chr_CRIBI
          start_CRIBI=input$start_CRIBI
          end_CRIBI=input$end_CRIBI
          strand_CRIBI=input$strand_CRIBI
          if(Genes_list_CRIBI == 'Chromosome coordinates' && chr_CRIBI != "" && start_CRIBI != "" && end_CRIBI != "" && strand_CRIBI != ""){
            regions.gr_CRIBI <- GRanges(seqnames=chr_CRIBI, ranges=IRanges(start=as.numeric(start_CRIBI), end=as.numeric(end_CRIBI)), strand=strand_CRIBI)
            # overlap entre gff et région donnée
            overlapGenes_CRIBI <- findOverlaps(regions.gr_CRIBI, gffRangedData_CRIBI)
            overlapGenes.df_CRIBI <- as.data.frame(overlapGenes_CRIBI)
            # liste des gènes dans la région donnée
            genes_CRIBI=unique(names(gffRangedData_CRIBI[overlapGenes.df_CRIBI$subjectHits]))
            output$selected_genes_CRIBI <- renderText({
              paste(as.character(genes_CRIBI), collapse=",")
            })
            data_CRIBI_samples_log <- data_CRIBI_samples_log[row.names(data_CRIBI_samples_log) %in% genes_CRIBI, ]
            heatmaply(data_CRIBI_samples_log,dendrogram = "none",margins=c((max(nchar(colnames(data_CRIBI_samples_log)))*4),150+(length(colnames(data_CRIBI_samples_log))/20),2,2),fontsize_row=7,fontsize_col=7,colors = colorRampPalette(rev(brewer.pal(n = 7, name = "RdYlBu")))(100),labCol=colnames(data_CRIBI_samples_log),labRow=rownames(data_CRIBI_samples_log)) %>% layout(height=as.numeric(input$dimension[2]),width=(0.8*as.numeric(input$dimension[1])))
          } else {
            genes_CRIBI=unlist(strsplit(input$list_CRIBI,','))
            if(length(genes_CRIBI) != 0){
              data_CRIBI_samples_log <- data_CRIBI_samples_log[row.names(data_CRIBI_samples_log) %in% genes_CRIBI, ]
              heatmaply(data_CRIBI_samples_log,dendrogram = "none",margins=c((max(nchar(colnames(data_CRIBI_samples_log)))*4),150+(length(colnames(data_CRIBI_samples_log))/20),2,2),fontsize_row=7,fontsize_col=7,colors = colorRampPalette(rev(brewer.pal(n = 7, name = "RdYlBu")))(100),labCol=colnames(data_CRIBI_samples_log),labRow=rownames(data_CRIBI_samples_log)) %>% layout(height=as.numeric(input$dimension[2]),width=(0.8*as.numeric(input$dimension[1])))
            } else {
              shinyalert(title = "You genes ID list is empty, please fill it.", type = "warning")
            }
          }
        }
      })
  }

})

#--------------------------------------------------------------------------------
